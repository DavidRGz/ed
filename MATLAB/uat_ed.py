from random import *

var = 3
vector = []
rango = [[-5,5],[-5,5],[-1,10]]

NP = 20
F = 0.8
CR = 0.5
genmax = 10000

for k in range(NP):
	vector.append([None]*(var+1))

for i in range(NP):
	for j in range(var):
		vector[i][j] = rango[j][0] + (rango[j][1] - rango[j][0])*random()
	vector[i][var] = vector[i][0]**2 + vector[i][1]**2 + vector[i][2]**2
vectruido = vector
for m in range(genmax):
	print "Generacion:", m
	for i in range(NP):
		r1 = randint(0,NP-1)
		while(r1==i):
			r1 = randint(0,NP-1)
		r2 = randint(0,NP-1)
		while(r2==i or r2==r1):
			r2 = randint(0,NP-1)
		r3 = randint(0,NP-1)
		while(r3==i or r3==r2 or r3==r1):
			r3 = randint(0,NP-1)
		for k in range(var):
			vectruido[0][k] = vector[r1][k] + F*(vector[r2][k] - vector[r3][k])
			if vectruido[0][k] < rango[k][0]:
				vectruido[0][k] = (2*rango[k][0]) - vectruido[0][k]
			else:
				if vectruido[0][k] > rango[k][1]:
					vectruido[0][k] = (2*rango[k][1]) - vectruido[0][k]
		jrand = randint(0,var-1)
		hijo = vectruido
		for k in range(var):
			randj = random()
			if (randj<CR or jrand==k):
				hijo[0][k] = vectruido[0][k]
			else:
				hijo[0][k] = vector[i][k]
		hijo[0][var] = hijo[0][0]**2 + hijo[0][1]**2 + hijo[0][2]**2
		if vector[i][var]>hijo[0][var]:
			vector[i][:] = hijo[0][:]
			#for ii in range(len(vector[i])):
			#	vector[i][ii] = hijo[0][ii]
for k in range(NP):
	print vector[k][0], vector[k][1], vector[k][2], vector[k][3]