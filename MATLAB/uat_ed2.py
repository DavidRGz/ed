from random import *
from math import *

var = 2
vector = []
rango = [[0,5],[0,5]]

eps = 0.00001
NP = 20
a = 0.5
b = 1.0
CR = a + (b-a)*random()
genmax = 100000

for k in range(NP):
	vector.append([None]*(var+3))

for i in range(NP):
	for j in range(var):
		vector[i][j] = rango[j][0] + (rango[j][1] - rango[j][0])*random()
	vector[i][var] = pi*vector[i][0]**2*vector[i][1]
	vector[i][var+1] = 2*pi*vector[i][0]**2 + 2*pi*vector[i][0]*vector[i][1] - 24*pi
	hxvector = abs(vector[i][var+1])
	if hxvector > eps:
		vector[i][var+2] = hxvector
	else:
		vector[i][var+2] = 0

vectruido = vector
for m in range(genmax):
	print "Generacion:", m
	a = 0.3
	b = 0.9
	F = a + (b-a)*random()
	for i in range(NP):
		r1 = randint(0,NP-1)
		while(r1==i):
			r1 = randint(0,NP-1)
		r2 = randint(0,NP-1)
		while(r2==i or r2==r1):
			r2 = randint(0,NP-1)
		r3 = randint(0,NP-1)
		while(r3==i or r3==r2 or r3==r1):
			r3 = randint(0,NP-1)
		for k in range(var):
			vectruido[0][k] = vector[r1][k] + F*(vector[r2][k] - vector[r3][k])
			if vectruido[0][k] < rango[k][0]:
				vectruido[0][k] = (2*rango[k][0]) - vectruido[0][k]
			else:
				if vectruido[0][k] > rango[k][1]:
					vectruido[0][k] = (2*rango[k][1]) - vectruido[0][k]
		jrand = randint(0,var-1)
		hijo = vectruido
		for k in range(var):
			randj = random()
			if (randj<CR or jrand==k):
				hijo[0][k] = vectruido[0][k]
			else:
				hijo[0][k] = vector[i][k]
		hijo[0][var] = pi*hijo[0][0]**2*hijo[0][1]
		hijo[0][var+1] = 2*pi*hijo[0][0]**2 + 2*pi*hijo[0][0]*hijo[0][1] - 24*pi
		hxhijo = abs(hijo[0][var+1])
		hx1 = hxhijo - eps
		if hx1 > 0:
			hijo[0][var+2] = abs(hx1)
		else:
			hijo[0][var+2] = 0
		if (vector[i][var+2]>0 and hijo[0][var+2]>0):
			if vector[i][var+2]>=hijo[0][var+2]:
				vector[i][:] = hijo[0][:]
				#for ii in range(len(vector[i])):
				#	vector[i][ii] = hijo[0][ii]
		else:
			if (vector[i][var+2]<=0 and hijo[0][var+2]<=0):
				if vector[i][var]>=hijo[0][var]:
					vector[i][:] = hijo[0][:]
					#for ii in range(len(vector[i])):
					#	vector[i][ii] = hijo[0][ii]
		if (vector[i][var+2]>0 and hijo[0][var+2]==0):
			vector[i][:] = hijo[0][:]
			#for ii in range(len(vector[i])):
			#	vector[i][ii] = hijo[0][ii]
for k in range(NP):
	print vector[k][0], vector[k][1], vector[k][2], vector[k][3], vector[k][4]