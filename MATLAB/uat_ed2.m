close all;
clear all;

rand('twister',sum(100*clock));
var=2;
vector=[];
rango=[0 5
       0 5];
eps=0.00001;   
%Condiciones iniciales del algoritmo   
NP=20; %Cantidad de individuos en la poblacion
% F=0.8; %Factor de escalamiento o mutacion
% CR=0.5; %Factor de cruza.
 a=0.5;
 b=1.0;
 CR=a+(b-a)*rand(1);
genmax=100000; %Cantidad de generaciones de la ED.
%Condiciones iniciales del aloritmo

%Generando la primera poblacion
for i=1:NP
    for j=1:var
        vector(i,j)=rango(j,1)+(rango(j,2)-rango(j,1))*rand(1);
    end
    %Evaluando la funcion objetivo
    vector(i,var+1)=pi*vector(i,1)^2*vector(i,2);
    %Evaluando la restriccion
    vector(i,var+2)=2*pi*vector(i,1)^2+ 2*pi*vector(i,1)*vector(i,2)-24*pi;
    %Verificando el cero "gordo" de la restriccion
    hxvector=abs(vector(i,var+2));
    %hx1=hxvector-eps;
    if hxvector>eps
        vector(i,var+3)=hxvector;
    else
        vector(i,var+3)=0;
    end
end
%Generando la primera poblacion
vector

for m=1:genmax
    disp('generación')
    m
     a=0.3;
     b=0.9;
     F=a+(b-a)*rand(1); 
 for i=1:NP
     r1=randi(NP,1);
    while(r1==i)
      r1=randi(NP,1);    
    end
    r2=randi(NP,1);
    while(r2==i || r2==r1)
      r2=randi(NP,1);    
    end
    r3=randi(NP,1);
    while(r3==i || r3==r2 || r3==r1)
      r3=randi(NP,1);    
    end
    %Generamos el vector de ruido
  for k=1:var
    vectruido(1,k)=vector(r1,k)+F*(vector(r2,k)-vector(r3,k));
    %verifica limites
    if vectruido(1,k)<rango(k,1)
        vectruido(1,k)=(rango(k,1)*2)-vectruido(1,k);
    else
        if vectruido(1,k)>rango(k,2)
           vectruido(1,k)=(rango(k,2)*2)-vectruido(1,k);
        end
    end
  end
 %Generamos el vector de ruido
  jrand=randi(var,1);
  %Algoritmo principal de ED
  for k=1:var
    randj=rand(1);
    if (randj<CR || jrand==k)
        hijo(1,k)=vectruido(1,k);
    else
        hijo(1,k)=vector(i,k);
    end
  end
  %evaluar al hijo en la funcion
  hijo(1,var+1)=pi*hijo(1,1)^2*hijo(1,2);
  %evaluar restricciones 
  hijo(1,var+2)=2*pi*hijo(1,1)^2+ 2*pi*hijo(1,1)*hijo(1,2)-24*pi;
  %Verificando el cero "gordo" de la restricción
   hxhijo=abs(hijo(1,var+2));
   hx1=hxhijo-eps;
      if (hx1>0)
         hijo(1,var+3)=abs(hx1);
      else
          hijo(1,var+3)=0;
      end  
  %Reemplazo del padre por el hijo siguiendo reglas de Deb
  
  if (vector(i,var+3)>0) && (hijo(1,var+3)>0) %Si ambos violan restricciones
      if (vector(i,var+3)>=hijo(1,var+3)) %Se toma al que tiene menor SVR.
          vector(i,:)=hijo(1,:);          
      end
  else
      if (vector(i,var+3)<=0) && (hijo(1,var+3)<=0) %Si ambos no violan restricciones
          if vector(i,var+1)>=hijo(1,var+1)% Se toma al que tiene mejor F.O.
              vector(i,:)=hijo(1,:);
          end
      end
  end
  if (vector(i,var+3)>0) && (hijo(1,var+3)==0) %El padre viola y el hijo no viola
      vector(i,:)=hijo(1,:);
  end
 %Reemplazo del padre por el hijo siguiendo reglas de Deb. 
 end %Del ciclo de NP
end %Del ciclo de genmax

disp('Vector final')
% for i=1:NP
%    if vector(i,var+3)==0
%        vector(i,:)
%    end
% end  
vector






