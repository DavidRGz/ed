from random import *
from math import *

var = 3
res = 2
vector = []
rango = [[0,5],[0,5],[0,5]]

eps = 0.00001
NP = 100
F = 0.8
CR = 0.6
genmax = 10000

for k in range(NP):
	vector.append([None]*(var+res+2))

for i in range(NP):
	for j in range(var):
		vector[i][j] = rango[j][0] + (rango[j][1] - rango[j][0])*random()
	#funcion objetivo
	vector[i][var] = vector[i][0]**2*vector[i][1] + 3*vector[i][2] - 6*vector[i][1] + 3*vector[i][0]
	#restricciones
	vector[i][var+1] = vector[i][1] - vector[i][0]**2 - 1
	vector[i][var+2] = vector[i][0] - vector[i][1] + vector[i][2] - 1
	vector[i][var+res+1] = 0
	for k in range(res):
		hv = abs(vector[i][var+k+1])
		cerog = hv-eps
		if cerog>0:
			vector[i][var+res+1] = vector[i][var+res+1] + abs(cerog) 

vectruido = vector
for m in range(genmax):
	print "Generacion:", m
	for i in range(NP):
		r1 = randint(0,NP-1)
		while(r1==i):
			r1 = randint(0,NP-1)
		r2 = randint(0,NP-1)
		while(r2==i or r2==r1):
			r2 = randint(0,NP-1)
		r3 = randint(0,NP-1)
		while(r3==i or r3==r2 or r3==r1):
			r3 = randint(0,NP-1)
		for k in range(var):
			vectruido[0][k] = vector[r1][k] + F*(vector[r2][k] - vector[r3][k])
			if vectruido[0][k] < rango[k][0]:
				vectruido[0][k] = (2*rango[k][0]) - vectruido[0][k]
			else:
				if vectruido[0][k] > rango[k][1]:
					vectruido[0][k] = (2*rango[k][1]) - vectruido[0][k]
		jrand = randint(0,var-1)
		hijo = vectruido
		for k in range(var):
			randj = random()
			if (randj<CR or jrand==k):
				hijo[0][k] = vectruido[0][k]
			else:
				hijo[0][k] = vector[i][k]
		hijo[0][var] = hijo[0][0]**2*hijo[0][1] + 3*hijo[0][2] - 6*hijo[0][1] + 3*hijo[0][0]
		hijo[0][var+1] = hijo[0][1] - hijo[0][0]**2 - 1
		hijo[0][var+2] = hijo[0][0] - hijo[0][1] + hijo[0][2] - 1
		hijo[0][var+res+1] = 0
		for k in range(res):
			hv = abs(hijo[0][var+k+1])
			cerog = hv-eps
			if cerog>0:
				hijo[0][var+res+1] = hijo[0][var+res+1] + abs(cerog)
		if (vector[i][var+res+1]>0 and hijo[0][var+res+1]>0):
			if vector[i][var+res+1]>=hijo[0][var+res+1]:
				vector[i][:] = hijo[0][:]
				#for ii in range(len(vector[i])):
				#	vector[i][ii] = hijo[0][ii]
		else:
			if (vector[i][var+res+1]<=0 and hijo[0][var+res+1]<=0):
				if vector[i][var]>=hijo[0][var]:
					vector[i][:] = hijo[0][:]
					#for ii in range(len(vector[i])):
					#	vector[i][ii] = hijo[0][ii]
		if (vector[i][var+res+1]>0 and hijo[0][var+res+1]==0):
			vector[i][:] = hijo[0][:]
			#for ii in range(len(vector[i])):
			#	vector[i][ii] = hijo[0][ii]
for k in range(NP):
	print round(vector[k][0],2), round(vector[k][1],2), round(vector[k][2],2), round(vector[k][3],2), round(vector[k][4],2), round(vector[k][5],2), round(vector[k][6],2)