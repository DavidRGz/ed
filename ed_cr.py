#Se importan los modulos necesarios: random, math y os
from random import *
from math import *
import os

def fa(_y):
	return -2.0/5.0*_y+4.0/7.0

def fb(_y):
	return 3.0/4.0*_y+1.0/6.0

def yk(_N, _xk):
	_suma = 0
	for _k in range(len(_xk)):
		_suma += int(_xk[_k])
	return 1.0/_N*_suma

#Funcion para limpiar pantalla, segun el sistema operativo
def clearscreen():
	if os.name == "nt":
		os.system("cls")
	elif os.name == "posix":
		os.system("clear")

#Funcion para evaluar las restricciones de desigualdad
#Recibe una expresion de tipo string
#Retorna la diferencia numerica requerida para cumplir la condicion
def eval_res_g(_expresion):
	if eval(_expresion):
		return 0
	else:
		_exp_der,_op,_exp_izq = _expresion.split(" ")
		if _op == "<" or _op == ">":
			return abs(eval(_exp_izq)-eval(_exp_der))+1
		else:
			return abs(eval(_exp_izq)-eval(_exp_der))

#Funcion para evaluar las restricciones de igualdad _a cero
#Recibe una expresion de tipo string y el cero "gordo" _a considerar
#Retorna la diferencia numerica requerida para cumplir la condicion
def eval_res_h(_expresion,_eps):
	if abs(eval(_expresion)) >= _eps:
		return abs(eval(_expresion)) - _eps
	else:
		return 0

#Condiciones iniciales
_var = []
_rango = []
_fo = []
_o = []
_g = []
_h = []
_eps = []
_ec = []

"""
#Se pide al usuario el numero de variables, sus nombres y sus rangos
_nvar = input("Numero de variables: ")
for _k in range(_nvar):
	_var.append(raw_input("Nombre de la variable " + str(_k+1) + ": "))
	_ri = input("_Rango inicial: ")
	_rf = input("_Rango final: ")
	_rango.append([_ri,_rf])
clearscreen()

#Se pide al usuario el numero de funciones ob_jetivo,
#sus expresiones y si se desea minimizar _o maximizar
_nfo = input("Numero de funciones ob_jetivo: ")
for _k in range(_nfo):
	_fo.append(raw_input("f" + str(_k+1) + "(x) = "))
	print "Escribir min para minimizar _o max para maximizar"
	_o.append(raw_input("Ob_jetivo: "))
clearscreen()

#Se pide al usuario el numero de restricciones de desigualdad y sus expresiones
_ng = input("Numero de restricciones de desigualdad _g(x): ")
if _ng >= 1: print "Escribir por e_jemplo: x1 + x2 <= 0"
for _k in range(_ng):
	_g.append(raw_input("_g" + str(_k+1) + "(x): "))
clearscreen()

#Se pide al usuario el numero de restricciones de igualdad _a cero,
#sus expresiones y el valor de cero _a considerar
_nh = input("Numero de restricciones de igualdad _h(x) = 0: ")
if _nh >= 1: print "Escribir por e_jemplo: x1 + x2"
for _k in range(_nh):
	_h.append(raw_input("_h" + str(_k+1) + "(x): "))
	_eps.append(input("Valor de cero _a considerar: "))
clearscreen()

#Se pide al usuario el numero de ecuaciones adicionales y sus expresiones
#Valgase para establecer valores que simplifiquen la _expresion de la funcion ob_jetivo
#Por e_jemplo: c = pi*sin(10+x) + cos(10+x)
_nec = input("Numero de ecuaciones adicionales: ")
for _k in range(_nec):
	_ec.append(raw_input("Ecuacion " + str(_k+1) + ": "))
clearscreen()
"""

_file = open(raw_input("url: "))
_data = _file.read()
_file.close()
_vector_data = _data.split("$")
_temp = _vector_data[0].split(";")
for _k in range(len(_temp)):
	if _temp[_k] != "":
		_fo.append(_temp[_k].split(" ")[1])
		_o.append(_temp[_k].split(" ")[0])
_temp = _vector_data[1].split(";")
for _k in range(len(_temp)):
	if _temp[_k] != "":
		_var.append(_temp[_k].split("<=")[1])
		_ri = float(_temp[_k].split("<=")[0])
		_rf = float(_temp[_k].split("<=")[2])
		_rango.append([_ri,_rf])
_temp = _vector_data[2].split(";")
for _k in range(len(_temp)):
	if _temp[_k] != "":
		_g.append(_temp[_k])
_temp = _vector_data[3].split(";")
for _k in range(len(_temp)):
	if _temp[_k] != "":
		_h.append(_temp[_k].split(" = ")[0])
		_eps.append(_temp[_k].split(" = ")[1])
_temp = _vector_data[4].split(";")
for _k in range(len(_temp)):
	if _temp[_k] != "":
		_ec.append(_temp[_k])

_nfo = len(_fo)
_nvar = len(_var)
_ng = len(_g)
_nh = len(_h)
_nec = len(_ec)

#Se pide al usuario el numero de poblacion y de generaciones
_np = input("Numero de individuos en la poblacion: ")
_genmax = input("Numero de generaciones: ")
clearscreen()

#Se genera la primera poblacion
_vector = []
_vectruido = []
for _k in range(_np):
	_vector.append([None]*(_nvar+_nfo+1))
for _k in range(_nvar):
	_vectruido.append(None)

for _i in range(_np):
	for _j in range(_nvar):
		_vector[_i][_j] = _rango[_j][0] + (_rango[_j][1] - _rango[_j][0])*random()
		exec(_var[_j] + " = _vector[_i][_j]")
	#Se e_jecutan las ecuaciones adicionales
	for _k in range(_nec):
		exec(_ec[_k])
	#Se evaluan las funciones ob_jetivo
	for _k in range(_nfo):
		_vector[_i][_nvar+_k] = eval(_fo[_k])
	#Se evaluan las restricciones
	_svr = 0
	for _k in range(_ng):
		_svr += eval_res_g(_g[_k])
	for _k in range(_nh):
		_svr += eval_res_h(_h[_k],_eps[_k])
	_vector[_i][_nvar+_nfo] = _svr

#Comienza algoritmo de Evolucion Diferencial
for _m in range(_genmax):
	print "Generacion:", _m
	_x = []
	_x.append(True) #A
	_cr = []
	_a = 0.3
	_b = 0.9
	_fm = _a + (_b-_a)*random()
	for _i in range(_np):
		_r1 = randint(0,_np-1)
		while(_r1==_i):
			_r1 = randint(0,_np-1)
		_r2 = randint(0,_np-1)
		while(_r2==_i or _r2==_r1):
			_r2 = randint(0,_np-1)
		_r3 = randint(0,_np-1)
		while(_r3==_i or _r3==_r2 or _r3==_r1):
			_r3 = randint(0,_np-1)

		#Se genera el _vector ruido
		for _k in range(_nvar):
			_vectruido[_k] = _vector[_r1][_k] + _fm*(_vector[_r2][_k] - _vector[_r3][_k])
			#Verifica limites _o rangos de cada variable
			if _vectruido[_k] < _rango[_k][0]:
				_vectruido[_k] = (2*_rango[_k][0]) - _vectruido[_k]
			else:
				if _vectruido[_k] > _rango[_k][1]:
					_vectruido[_k] = (2*_rango[_k][1]) - _vectruido[_k]

		_hijo = []
		for _k in range(_nvar+_nfo+1):
			_hijo.append(None)

		#Modelo de toma de decision binario
		if fa(yk(_np,_x)) > fb(yk(_np,_x)):
			_x.append(_x[-1])
			_cr.append(fa(yk(_np,_x)))
		else:
			_x.append(not _x[-1])
			_cr.append(fb(yk(_np,_x)))
		_cr[-1] = _cr[-1]/max(_cr)

		#Algoritmo principal de ED
		for _k in range(_nvar):
			if (_cr[-1]>0.5):
				_hijo[_k] = _vectruido[_k]
				exec(_var[_k] + " = _vectruido[_k]")
			else:
				_hijo[_k] = _vector[_i][_k]
				exec(_var[_k] + " = _vector[_i][_k]")

		#Se ejecutan las ecuaciones adicionales
		for _k in range(_nec):
			exec(_ec[_k])
		#Se evaluan las funciones ob_jetivo
		for _k in range(_nfo):
			_hijo[_nvar+_k] = eval(_fo[_k])
		#Se evaluan las restricciones
		_svr = 0
		for _k in range(_ng):
			_svr += eval_res_g(_g[_k])
		for _k in range(_nh):
			_svr += eval_res_h(_h[_k],_eps[_k])
		_hijo[_nvar+_nfo] = _svr

		#Reemplazo del padre por el hijo siguiendo las reglas de Deb
		#Si ambos violan restricciones
		if (_vector[_i][_nvar+_nfo]>0 and _hijo[_nvar+_nfo]>0):
			#Se toma al que tiene menor SVR
			if _vector[_i][_nvar+_nfo]>=_hijo[_nvar+_nfo]:
				_vector[_i][:] = _hijo[:]
		else:
			#Si ambos no violan restricciones
			if (_vector[_i][_nvar+_nfo]<=0 and _hijo[_nvar+_nfo]<=0):
				for _k in range(_nfo):
					if _o[_k] == "Minimizar":
						#Se toma al que tiene mejor (menor) F._O.
						if _vector[_i][_nvar+_k]>=_hijo[_nvar+_k]:
							_vector[_i][:] = _hijo[:]
					else:
						#Se toma al que tiene mejor (mayor) F._O.
						if _vector[_i][_nvar+_k]<=_hijo[_nvar+_k]:
							_vector[_i][:] = _hijo[:]
		#El padre viola restricciones y el _hijo no
		if (_vector[_i][_nvar+_nfo]>0 and _hijo[_nvar+_nfo]==0):
			_vector[_i][:] = _hijo[:]

#Se despliegan los valores de las variables, funciones ob_jetivo y _svr
#De cada uno de los individuos de la poblacion
_vector_string = ""
for _i in range(_nvar+_nfo+1):
	_vector_string += "_vector[_k][" + str(_i) + "] ,"
_vector_string = _vector_string[:-1]
for _k in range(_np):
	exec("print " + _vector_string)