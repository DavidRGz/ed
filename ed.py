#Se importan los modulos necesarios: random, math y os
from random import *
from math import *
import os

#Funcion para limpiar pantalla, segun el sistema operativo
def clearscreen():
	if os.name == "nt":
		os.system("cls")
	elif os.name == "posix":
		os.system("clear")

#Funcion para evaluar las restricciones de desigualdad
#Recibe una expresion de tipo string
#Retorna la diferencia numerica requerida para cumplir la condicion
def eval_res_g(_expresion):
	if eval(_expresion):
		return 0
	else:
		_exp_der,_op,_exp_izq = _expresion.split(" ")
		return eval_res_h(str(abs(eval(_exp_izq)-eval(_exp_der))))

#Funcion para evaluar las restricciones de igualdad a cero
#Recibe una expresion de tipo string y el cero "gordo" a considerar
#Retorna la diferencia numerica requerida para cumplir la condicion
def eval_res_h(_expresion,_eps=0):
	if abs(eval(_expresion)) >= _eps:
		return abs(eval(_expresion)) - _eps
	else:
		return 0

#Condiciones iniciales
_var = []
_rango = []
_fo = []
_o = []
_g = []
_h = []
_eps = []
_ec = []
_a = 0.5
_b = 1.0
_cr = _a + (_b-_a)*uniform(0,1)

################## Esta seccion solicita los datos de un nuevo ejercicio de optimizacion ##################
##################          No es necesaria para abrir ejercicios ya creados             ##################
"""
print "A continuacion se solicitan los datos para un nuevo ejercicio de optimiazcion"
#Se pide al usuario el numero de variables, sus nombres y sus rangos
_nvar = input("Numero de variables: ")
for _k in range(_nvar):
	_var.append(raw_input("Nombre de la variable " + str(_k+1) + ": "))
	_ri = input("_Rango inicial: ")
	_rf = input("_Rango final: ")
	_rango.append([_ri,_rf])
clearscreen()

#Se pide al usuario el numero de funciones objetivo,
#sus expresiones y si se desea minimizar o maximizar
_nfo = input("Numero de funciones ob_jetivo: ")
for _k in range(_nfo):
	_fo.append(raw_input("f" + str(_k+1) + "(x) = "))
	print "Escribir min para minimizar _o max para maximizar"
	_o.append(raw_input("Ob_jetivo: "))
clearscreen()

#Se pide al usuario el numero de restricciones de desigualdad y sus expresiones
_ng = input("Numero de restricciones de desigualdad _g(x): ")
if _ng >= 1: print "Escribir por e_jemplo: x1 + x2 <= 0"
for _k in range(_ng):
	_g.append(raw_input("_g" + str(_k+1) + "(x): "))
clearscreen()

#Se pide al usuario el numero de restricciones de igualdad a cero,
#sus expresiones y el valor de cero a considerar
_nh = input("Numero de restricciones de igualdad _h(x) = 0: ")
if _nh >= 1: print "Escribir por e_jemplo: x1 + x2"
for _k in range(_nh):
	_h.append(raw_input("_h" + str(_k+1) + "(x): "))
	_eps.append(input("Valor de cero _a considerar: "))
clearscreen()

#Se pide al usuario el numero de ecuaciones adicionales y sus expresiones
#Valgase para establecer valores que simplifiquen la expresion de la funcion objetivo
#Por e_jemplo: c = pi*sin(10+x) + cos(10+x)
_nec = input("Numero de ecuaciones adicionales: ")
for _k in range(_nec):
	_ec.append(raw_input("Ecuacion " + str(_k+1) + ": "))
clearscreen()
"""
###########################################################################################################

##################     Esta seccion solicita la url de un ejercicio de optimizacion     ##################
##################                   Para abrir ejercicios ya creados                   ##################
print "A continuacion se solicitan los datos para optimizar un ejercicio ya creado"
print "Escribe la url del archivo txt que se desea abrir para optimizar, por ejemplo: ejercicios/g01.txt"
_file = open(raw_input("url: "))
_data = _file.read()
_file.close()
_vector_data = _data.split("$")
_temp = _vector_data[0].split(";")
for _k in range(len(_temp)):
	if _temp[_k] != "":
		_fo.append(_temp[_k].split(" ")[1])
		_o.append(_temp[_k].split(" ")[0])
_temp = _vector_data[1].split(";")
for _k in range(len(_temp)):
	if _temp[_k] != "":
		_var.append(_temp[_k].split("<=")[1])
		_ri = float(_temp[_k].split("<=")[0])
		_rf = float(_temp[_k].split("<=")[2])
		_rango.append([_ri,_rf])
_temp = _vector_data[2].split(";")
for _k in range(len(_temp)):
	if _temp[_k] != "":
		_g.append(_temp[_k])
_temp = _vector_data[3].split(";")
for _k in range(len(_temp)):
	if _temp[_k] != "":
		_h.append(_temp[_k].split(" = ")[0])
		_eps.append(_temp[_k].split(" = ")[1])
_temp = _vector_data[4].split(";")
for _k in range(len(_temp)):
	if _temp[_k] != "":
		_ec.append(_temp[_k])

_nfo = len(_fo)
_nvar = len(_var)
_ng = len(_g)
_nh = len(_h)
_nec = len(_ec)

###########################################################################################################

print "Para la optimizacion elige el numero de individuos y generaciones"
#Se pide al usuario el numero de poblacion y de generaciones
_np = input("Numero de individuos en la poblacion: ")
_genmax = input("Numero de generaciones: ")
clearscreen()

#Se genera la primera poblacion
_vector = []
_vectruido = []
for _k in range(_np):
	_vector.append([None]*(_nvar+_nfo+1))
for _k in range(_nvar):
	_vectruido.append(None)

for _i in range(_np):
	for _j in range(_nvar):
		_vector[_i][_j] = _rango[_j][0] + (_rango[_j][1] - _rango[_j][0])*uniform(0,1)
		exec(_var[_j] + " = _vector[_i][_j]")
	#Se ejecutan las ecuaciones adicionales
	for _k in range(_nec):
		exec(_ec[_k])
	#Se evaluan las funciones objetivo
	for _k in range(_nfo):
		_vector[_i][_nvar+_k] = eval(_fo[_k])
	#Se evaluan las restricciones
	_svr = 0
	for _k in range(_ng):
		_svr += eval_res_g(_g[_k])
	for _k in range(_nh):
		_svr += eval_res_h(_h[_k],_eps[_k])
	_vector[_i][_nvar+_nfo] = _svr

#Comienza algoritmo de Evolucion Diferencial
for _m in range(_genmax):
	print "Generacion:", _m
	_a = 0.3
	_b = 0.9
	_fm = _a + (_b-_a)*uniform(0,1)
	for _i in range(_np):
		_r1 = randint(0,_np-1)
		while(_r1==_i):
			_r1 = randint(0,_np-1)
		_r2 = randint(0,_np-1)
		while(_r2==_i or _r2==_r1):
			_r2 = randint(0,_np-1)
		_r3 = randint(0,_np-1)
		while(_r3==_i or _r3==_r2 or _r3==_r1):
			_r3 = randint(0,_np-1)

		#Se genera el vector ruido
		for _k in range(_nvar):
			_vectruido[_k] = _vector[_r1][_k] + _fm*(_vector[_r2][_k] - _vector[_r3][_k])
			#Verifica los limites de cada variable
			if _vectruido[_k] < _rango[_k][0]:
				_vectruido[_k] = _rango[_k][0]
			elif _vectruido[_k] > _rango[_k][1]:
				_vectruido[_k] = _rango[_k][1]

		_jrand = randint(0,_nvar-1)
		_hijo = []
		for _k in range(_nvar+_nfo+1):
			_hijo.append(None)

		#Algoritmo principal de ED
		for _k in range(_nvar):
			_randj = uniform(0,1)
			if (_randj<_cr or _jrand==_k):
				_hijo[_k] = _vectruido[_k]
				exec(_var[_k] + " = _vectruido[_k]")
			else:
				_hijo[_k] = _vector[_i][_k]
				exec(_var[_k] + " = _vector[_i][_k]")

		#Se ejecutan las ecuaciones adicionales
		for _k in range(_nec):
			exec(_ec[_k])
		#Se evaluan las funciones objetivo
		for _k in range(_nfo):
			_hijo[_nvar+_k] = eval(_fo[_k])
		#Se evaluan las restricciones
		_svr = 0
		for _k in range(_ng):
			_svr += eval_res_g(_g[_k])
		for _k in range(_nh):
			_svr += eval_res_h(_h[_k],_eps[_k])
		_hijo[_nvar+_nfo] = _svr

		#Reemplazo del padre por el hijo siguiendo las reglas de Deb
		#Si ambos violan restricciones
		if (_vector[_i][_nvar+_nfo]>0 and _hijo[_nvar+_nfo]>0):
			#Se toma al que tiene menor SVR
			if _vector[_i][_nvar+_nfo]>=_hijo[_nvar+_nfo]:
				_vector[_i][:] = _hijo[:]
		#Si ambos no violan restricciones
		elif (_vector[_i][_nvar+_nfo]<=0 and _hijo[_nvar+_nfo]<=0):
			for _k in range(_nfo):
				if _o[_k] == "Minimizar":
					#Se toma al que tiene mejor (menor) F.O.
					if _vector[_i][_nvar+_k]>=_hijo[_nvar+_k]:
						_vector[_i][:] = _hijo[:]
				else:
					#Se toma al que tiene mejor (mayor) F.O.
					if _vector[_i][_nvar+_k]<=_hijo[_nvar+_k]:
						_vector[_i][:] = _hijo[:]
		#El padre viola restricciones y el hijo no
		elif (_vector[_i][_nvar+_nfo]>0 and _hijo[_nvar+_nfo]==0):
			_vector[_i][:] = _hijo[:]

#Se despliegan los valores de las variables, funciones objetivo y svr
#De cada uno de los individuos de la poblacion
#Se muestran tambien las ecuaciones adicionales
_vector_string = ""
for _i in range(_nvar+_nfo+1):
	_vector_string += "_vector[_k][" + str(_i) + "] ,"
_vector_string = _vector_string[:-1]
for _k in range(_np):
	exec("print " + _vector_string)
for _k in range(_nec):
	print _ec[_k].split("=")[0], "=", eval(_ec[_k].split("=")[1])